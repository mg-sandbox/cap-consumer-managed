
// The destination is called on fiorimodule level, there where the destination route is configured in the xs-app.json
var url = sap.ui.require.toUrl("mg/cap/cons/ma/fiorimodule") + "/provider-dest";

// Load library enables the namespace mg.cap.prov.sa and can be use application wide as an object reference
sap.ui.getCore().loadLibrary("mg.cap.prov.sa", url);

// For information purpose...
console.info("Load library: " + url);


sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
    "mg/cap/cons/ma/fiorimodule/model/models",
    "sap/ui/core/AppCacheBuster"
], function (UIComponent, Device, models, AppCacheBuster) {
    "use strict";

	return UIComponent.extend("mg.cap.cons.ma.fiorimodule.Component", {

		metadata: {
			manifest: "json"
        },
        
		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		}
	});
});
