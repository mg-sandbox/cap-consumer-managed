sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageToast"
    ],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, MessageToast) {

		"use strict";

		return Controller.extend("mg.cap.cons.ma.fiorimodule.controller.App", {
			onInit: function () {
            },

            handleDebuggerButton: function(){
                debugger;
                // This point will be used at runtime to explain library.js 
                // Enter in the console mg.cap.prov.sa
                // Check the object and notice goodbyeworld control is not available
                // Object mg.cap.prov.sa is onlye available in the code or during debug
            },
            
            handleAddButton: function(){
                var oContext = this.getView().byId("BookTable").getBinding("items").create({
                    "ID": 0, "title": "", "stock": 0
                });    
                oContext.created().then(function () {
                        MessageToast.show("Enter the name of the book...");
                    }, function (oError) {
                        MessageToast.show("Oops something went wrong...");
                    }
                );                
            },

            handleDeleteButton: function(oEvent){
                var oTable = this.getView().byId("BookTable");
                var oContext = oEvent.getParameters().listItem.getBindingContext();
                oContext.delete("$auto").then(function () {
                    oTable.removeSelections();
                    MessageToast.show("Book deleted");
                }, function (oError) {
                    MessageToast.show("Oops book not deleted...");
                });
            }

		});
	});
