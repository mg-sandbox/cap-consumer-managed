/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"mg/cap/cons/ma/fiorimodule/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
